package com.my.spring.auto;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
public interface Car {

    void run();
}
