package com.my.spring.auto;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
public class Boy {

    private final Car car;

    public Boy(Car car){
        this.car = car;
    }

    public void drive(){
        car.run();
    }
}