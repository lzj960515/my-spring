package com.my.spring.example;

import com.my.spring.design.Autowired;
import com.my.spring.design.Component;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
@Component
public class DaiJia {

    @Autowired
    private Boy boy;

    public void driver(){
        System.out.println("daijia");
        boy.driver();
    }
}
