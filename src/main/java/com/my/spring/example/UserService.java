package com.my.spring.example;

import com.my.spring.design.Autowired;
import com.my.spring.design.BeanPostProcessor;
import com.my.spring.design.Component;
import com.my.spring.design.InitializingBean;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
@Component
public class UserService implements InitializingBean {

    @Autowired
    private Boy boy;

    @Autowired(required = false)
    private User user;

    public void test(){
        System.out.println("boy:" + boy + ", user:" + user);
    }

    @Override
    public void afterPropertiesSet() {
        System.out.println("userService afterPropertiesSet");
    }
}
