package com.my.spring.example;

import com.my.spring.design.BeanPostProcessor;
import com.my.spring.design.Component;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
@Component
public class MyBeanPostProcessor implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) {
        if("userService".equals(beanName)){
            System.out.println("userService 初始化前");
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) {
        if("userService".equals(beanName)){
            System.out.println("userService 初始化后");
        }
        return bean;
    }
}
