package com.my.spring.design;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
public class ApplicationContext {

    private final BeanFactory beanFactory = new BeanFactory();
    private final Class<?> configClass;

    public ApplicationContext(Class<?> configClass) {
        this.configClass = configClass;
        refresh();
    }

    private void refresh() {
        synchronized (beanFactory) {
            new ConfigurationClassProcessor(beanFactory).scan(configClass);
            beanFactory.preInstantiateSingletons();
        }
    }

    public Object getBean(String beanName) {
        return beanFactory.getBean(beanName);
    }

    public <T> T getBean(String beanName, Class<T> type) {
        return (T) getBean(beanName);
    }
}
