package com.my.spring.design;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
public class BeanDefinition {

    private Class<?> beanClass;

    private String beanName;

    private String scope;

    private boolean lazy;

    public BeanDefinition(Class<?> type, String beanName){
        Scope scope = type.getAnnotation(Scope.class);
        if(scope != null){
            this.scope = scope.value();
        }
        if (type.isAssignableFrom(Lazy.class)){
            this.lazy = true;
        }
        this.beanClass = type;
        this.beanName = beanName;
    }

    public Class<?> getBeanClass() {
        return beanClass;
    }

    public void setBeanClass(Class<?> beanClass) {
        this.beanClass = beanClass;
    }

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    public boolean isSingleton() {
        return "singleton".equals(this.scope) || "".equals(this.scope);
    }

    public boolean isPrototype() {
        return "prototype".equals(this.scope);
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public boolean isLazy() {
        return lazy;
    }

    public void setLazy(boolean lazy) {
        this.lazy = lazy;
    }
}
