package com.my.spring.design;

import com.sun.istack.internal.Nullable;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
public interface BeanPostProcessor {

    @Nullable
    default Object postProcessBeforeInitialization(Object bean, String beanName) {
        return bean;
    }

    @Nullable
    default Object postProcessAfterInitialization(Object bean, String beanName) {
        return bean;
    }
}
