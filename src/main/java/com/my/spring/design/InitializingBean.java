package com.my.spring.design;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
public interface InitializingBean {

	void afterPropertiesSet();

}