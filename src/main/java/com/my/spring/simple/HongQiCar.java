package com.my.spring.simple;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
public class HongQiCar implements Car {

    @Override
    public void run(){
        System.out.println("hongqi running");
    }
}