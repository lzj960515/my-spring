package com.my.spring.simple;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
public interface Car {

    void run();
}
