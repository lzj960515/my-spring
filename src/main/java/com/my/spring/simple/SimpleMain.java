package com.my.spring.simple;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
public class SimpleMain {

    private static Map<String, Object> map = new ConcurrentHashMap<>();

    public static void main(String[] args) {
        // 放
        map.put("car", new HongQiCar());

        // 使用
        new Boy((Car) map.get("car")).drive();
    }
}
