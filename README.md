# 手写Spring实现
见com.my.spring.design包

1、扫描 ✔

2、创建Bean ✔

3、BeanPostProcessor 初始化前、后 ✔

4、Aware回调 ✔

5、支持多类型注入

6、构造方法推断

7、循环依赖问题

8、代理

